export interface PhylosRegister {
    name: string; //Registername
    value: number; //Register value
    writable: boolean; //is register writable
}
