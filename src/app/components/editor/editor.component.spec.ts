import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorComponent } from './editor.component';

import {imports, declarations} from '../../app.module';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';
import { GoldenLayoutContainer } from 'ngx-golden-layout';

class mockGoldenLayoutContainer {

}

describe('EditorComponent', () => {
  let component: EditorComponent;
  let fixture: ComponentFixture<EditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: declarations,
      imports: imports,
      providers: [
        PhylosModelStateService, 
        {provide: GoldenLayoutContainer, useClass: mockGoldenLayoutContainer}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    (window as any).define = {amd: true};
    fixture = TestBed.createComponent(EditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
