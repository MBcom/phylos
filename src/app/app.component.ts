import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { of } from 'rxjs';
import { PhylosModelStateService } from './services/phylos-model-state.service';
import { GoldenLayoutComponent } from 'ngx-golden-layout';
import { PhylosDebugState } from './enums/phylos-debug-state.enum';
import { PhylosTheme } from './enums/phylos-theme.enum';

//To pass a layout into the golden-layout component
const INITIAL_LAYOUT /*= GoldenLayout.Config*/ = {
  dimensions: {
    headerHeight: 42
  },
  content: [{
    type: 'row',
    content: [{
      type: 'column',//left column
      width: 60,
      content: [{
        type: 'component',
        componentName: 'EditorComponent', // The name defined in componentTypes in app.module.ts
        title: 'Code-Editor',
        height: 70
      }, {
        type: 'component',
        componentName: 'ErrorListComponent', // The name defined in componentTypes in app.module.ts
        title: 'Problems <span class="badge badge-secondary" id="phylos_problemCounter">0</span>',
      }]
    }, {
      type: 'column',//right column
      content: [{
        type: 'stack',
        content: [{
          type: 'component',
          componentName: 'RegisterComponent', // The name defined in componentTypes in app.module.ts
          title: 'Register',
        },
        {
          type: 'component',
          componentName: 'IoRegisterComponent', // The name defined in componentTypes in app.module.ts
          title: 'I/O Register',
        },
        {
          type: 'component',
          componentName: 'RomComponent', // The name defined in componentTypes in app.module.ts
          title: 'ROM',
        }]
      }, {
        type: 'component',
        componentName: 'RamComponent', // The name defined in componentTypes in app.module.ts
        title: 'RAM',
      }]
    }]
  }]
};


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  //config for GoldenLayout - initial Fensteraufteilung
  //if saved to localstorage - restore
  layoutConfig$ = localStorage.getItem('phylos_layout') ? of(JSON.parse(localStorage.getItem('phylos_layout'))) : of(INITIAL_LAYOUT);

  //reference to golden layout api
  @ViewChild(GoldenLayoutComponent, { static: false })
  goldenLayout: GoldenLayoutComponent;


  //status of the components rendered in golden layout
  componentStatus: { [name: string]: boolean } = {
    RamComponent: true,
    RomComponent: true,
    RegisterComponent: true,
    ErrorListComponent: true,
    EditorComponent: true,
    IoRegisterComponent: true
  };

  constructor(
    public phylosState: PhylosModelStateService
  ) {

  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    if (this.goldenLayout) {
      //initialize current component status correctly
      var curConfig = JSON.stringify(this.goldenLayout.getGoldenLayoutInstance().toConfig());
      for (let k of Object.keys(this.componentStatus)) {
        this.componentStatus[k] = curConfig.indexOf(k) >= 0;
      }

      //listen for Golden Layout Events
      //see https://golden-layout.com/docs/GoldenLayout.html
      this.goldenLayout.getGoldenLayoutInstance().on('itemCreated', (ev) => {
        //window is created
        if (ev.type == "component" && ev.isComponent) {
          this.componentStatus[ev.componentName] = false;
        }
      });
      this.goldenLayout.getGoldenLayoutInstance().on('itemDestroyed', (ev) => {
        //window is destroyed
        if (ev.type == "component" && ev.isComponent) {
          this.componentStatus[ev.componentName] = false;
        }
      });


      //set theme on popouts
      this.goldenLayout.getGoldenLayoutInstance().on('windowOpened', (ev) => {
        this.phylosState.SetTheme(this.phylosState.currentTheme);
        this.goldenLayout.getGoldenLayoutInstance().openPopouts.forEach(p => {
          switch (this.phylosState.currentTheme) {
            case PhylosTheme["vs-dark"]:
              p.getWindow().document.body.className = 'pyhlos-theme-dark';
              break;
            default:
              p.getWindow().document.body.className = 'pyhlos-theme-light';
              break;
          }

        })
      })

      //save golden layout config to local storage
      this.goldenLayout.getGoldenLayoutInstance().on('stateChanged', () => {
        if (this.phylosState.isLoggedIn) {
          try {
            var conf = this.goldenLayout.getGoldenLayoutInstance().toConfig();
            //error fix for maximized items drawed incorrectly after reload
            conf.maximisedItemId = undefined;
            var state = JSON.stringify(conf);
            localStorage.setItem('phylos_layout', state);
          } catch (error) {
          }
        }
        //redraw correct after minimize
        this.goldenLayout.getGoldenLayoutInstance().updateSize();
      })
    }
  }

  /**
   * Resets golden layout to initial state
   */
  ResetLayout(): void {
    var state = JSON.stringify(INITIAL_LAYOUT);
    localStorage.setItem('phylos_layout', state);
    window.location.reload();
  }

  /**
   * Function to close/ open golden layout components
   * Used for the "View" Menu
   * @param coponentName Name of the Component as described in INITIAL_LAYOUT
   * @param visible
   */
  changeComponentWindow(coponentName: string, visible: boolean) {
    //look for real state
    var curConfig = JSON.stringify(this.goldenLayout.getGoldenLayoutInstance().toConfig());
    this.componentStatus[coponentName] = curConfig.indexOf(coponentName) >= 0;
    if (this.componentStatus[coponentName] == visible){
      //fix wrong states
      visible = !visible;
    }
    this.componentStatus[coponentName] = visible;
    if (this.goldenLayout) {
      if (visible) {
        //add
        this.goldenLayout.createNewComponent({
          componentName: coponentName,
          type: 'component',
          title: coponentName == "RamComponent" ? "RAM" : coponentName == "EditorComponent" ? "Code-Editor" : coponentName == "IoRegisterComponent" ? "I/O Register" : coponentName.replace('Component', '')
        })
      } else {
        //destroy
        (this.goldenLayout.getGoldenLayoutInstance().root.getItemsByFilter((f: any) => f.isComponent && f.componentName == coponentName)[0] as any).close();
      }
    } else {
      //reset state
      this.componentStatus[coponentName] = !visible;
    }
  }

  /**
   * Open file dialog
   * @param files 
   */
  handleFileInput(files: FileList) {
    var file = files.item(0);
    var reader = new FileReader();
    //after read complete set data to model
    reader.onload = (ev: any) => {
      this.phylosState.phylosModel.code = ev.target.result;
    }
    //start reading
    reader.readAsText(file);
  }

  /**
   * Open file dialog for RAM images
   * @param files 
   */
  handleFileInputRam(files: FileList) {
    var file = files.item(0);
    var reader = new FileReader();
    //after read complete set data to model
    reader.onload = (ev: any) => {
      var csv = ev.target.result.split(/\r?\n/);
      var realDebugMode = this.phylosState.phylosModel.isDebugMode + 0;
      csv.forEach((l, i, a) => {
        var data = l.split(';');
        //skip lines with wrong format
        if (isNaN(data[0] as any) || isNaN(data[1] as any)) return;

        if (i + 1 == a.length) {
          //trigger change in ram component
          this.phylosState.phylosModel.isDebugMode = PhylosDebugState.debugRunning;
        }

        this.phylosState.phylosModel.RAM.set(parseInt(data[0]), parseInt(data[1]));

        if (i + 1 == a.length) {
          //trigger change in ram component
          this.phylosState.phylosModel.isDebugMode = realDebugMode;
        }
      })
    }
    //start reading
    reader.readAsText(file);
  }

  /**
   * Function for download editor content
   */
  SaveAs(): void {
    var file = new Blob([this.phylosState.phylosModel.code], { type: "text/plain" });

    if (window.navigator.msSaveOrOpenBlob) // IE10+
      window.navigator.msSaveOrOpenBlob(file, "phylos-code.txt");
    else { // Others
      var a = document.createElement("a"),
        url = URL.createObjectURL(file);
      a.href = url;
      a.download = "phylos-code.txt";
      document.body.appendChild(a);
      a.click();
      setTimeout(function () {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  }

  /**
   * Resets phylos to initial state
   */
  Reset(): void {
    //reset code
    this.phylosState.phylosModel.code = "";

    //clear ram
    this.phylosState.phylosModel.RAM.clear();

    //clear breakpoints
    this.phylosState.phylosModel.breakpoints.clear();

    //reset debug state
    this.phylosState.phylosModel.isDebugMode = PhylosDebugState.noDebug;

    //reset register
    this.phylosState.phylosModel.register.forEach(r => {
      r.value = 0;
    })
  }
}
