import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IoRegisterComponent } from './io-register.component';
import { declarations, imports } from 'src/app/app.module';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';

describe('IoRegisterComponent', () => {
  let component: IoRegisterComponent;
  let fixture: ComponentFixture<IoRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: declarations,
      imports: imports,
      providers: [
        PhylosModelStateService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IoRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
