# PHYLOS Documentation

## Menu
### File
* With `File > New` you can reset PYHLOS Register, RAM and Editor.
* You can open a text file into the editor with `File > Open`.
* With `File > Save as` you can download the editor content.
* You can open an RAM image with `File > Open predefined RAM values`. The file should have the following CSV format: 
```csv
<address as number>;<value as number>
```
A header line is allowed, but not necessary.  

### Edit
Menu for triggering undo and redo actions in editor.

### View
* here you can reopen a closed window
* change the theme between dark and light mode

## Supported commands
* ldd R[i] R[j] k - lädt in R[j] den Inhalt der durch R[i] + k adressierten Speicherzelle des Datenspeichers
* sto R[i] R[j] k - speichert den Inhalt von R[j] in die durch R[i]+k adressierte Speicherzelle
des Datenspeichers
* shl R[i] R[j] R[k] - führt einen logischen Linksshift auf dem Inhalt von R[i] um den in R[j]
spezizierten Wert durch und speichert das Ergebnis in R[k]
* shr R[i] R[j] R[k] - führt einen logischen Rechtsshift auf dem Inhalt von R[i] um den in R[j]
spezizierten Wert durch und speichert das Ergebnis in R[k]
* rol R[i] R[j] R[k] - führt eine logische Linksrotation auf dem Inhalt von R[i] um den in R[j]
spezizierten Wert durch und speichert das Ergebnis in R[k]
* ror R[i] R[j] R[k] - führt eine logische Rechtssrotation auf dem Inhalt von R[i] um den in
R[j] spezizierten Wert durch und speichert das Ergebnis in R[k]
* sub R[i] R[j] R[k] - subtrahiert von dem Inhalt aus R[i] den Inhalt aus R[j] und speichert das Ergebnis in R[k]
* add R[i] R[j] R[k] - addiert den Inhalt aus R[i] mit dem Inhalt aus R[j] und speichert das
Ergebnis in R[k]
* shli R[i] R[j] k - führt einen logischen Linksshift auf dem Inhalt von R[i] um den durch k
spezizierten Wert durch und speichert das Ergebnis in R[j]
* shri R[i] R[j] k - führt einen logischen Rechtsshift auf dem Inhalt von R[i] um den durch k
spezizierten Wert durch und speichert das Ergebnis in R[j]
* roli R[i] R[j] k - führt eine logische Linksrotation auf dem Inhalt von R[i] um den durch k
spezizierten Wert durch und speichert das Ergebnis in R[j]
* rori R[i] R[j] k - führt eine logische Rechtssrotation auf dem Inhalt von R[i] um den durch
k spezizierten Wert durch und speichert das Ergebnis in R[j]
* subi R[i] R[j] k - subtrahiert von dem Inhalt aus R[i] die Konstante k und speichert das
Ergebnis in R[j]
* addi R[i] R[j] k - addiert dem Inhalt aus R[i] die Konstante k hinzu und speichert das
Ergebnis in R[j]
* or R[i] R[j] R[k] - führt eine bitweise Disjunktion auf den Inhalten von R[i] und R[j] durch
und speichert das Ergebnis in R[k]
* and R[i] R[j] R[k] - führt eine bitweise Konjunktion auf den Inhalten von R[i] und R[j]
durch und speichert das Ergebnis in R[k]
* xor R[i] R[j] R[k] - führt eine bitweises Exklusiv-Oder auf den Inhalten von R[i] und R[j]
durch und speichert das Ergebnis in R[k]
* xnor R[i] R[j] R[k] - führt eine bitweise Aquivalenz auf den Inhalten von R[i] und R[j] durch
und speichert das Ergebnis in R[k]
* jmp k - ladt in den Befehlszähler die Konstante k; es wird also ein absoluter Sprung zur
Adresse k durchgeführt
* beq R[i] R[j] k - addiert auf den im Befehlszähler gespeicherten Wert die Konstante k, wenn
die Inhalte von R[i] und R[j] gleich sind; es wird ein relativer Sprung durchgeführt
* bneq R[i] R[j] k - addiert auf den im Befehlszähler gespeichertenWert die Konstante k, wenn
die Inhalte von R[i] und R[j] nicht gleich sind; es wird ein relativer Sprung durchgeführt
* bgt R[i] R[j] k - addiert auf den im Befehlszähler gespeicherten Wert die Konstante k,
wenn der Inhalt von R[i] größer als der Inhalt von R[j] ist; es wird ein relativer Sprung
durchgeführt
* bo k - addiert auf den im Befehlszähler gespeicherten Wert die Konstante k, wenn im zuvor
durchgeführten Befehl ein Überlauf aufgetreten ist; es wird ein relativer Sprung durchgeführt