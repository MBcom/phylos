import { Component, OnInit, ViewChild } from '@angular/core';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';
import { PhylosRam } from 'src/app/models/phylos-ram';
import { PhylosDebugState } from 'src/app/enums/phylos-debug-state.enum';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-ram',
  templateUrl: './ram.component.html',
  styleUrls: ['./ram.component.scss']
})
export class RamComponent implements OnInit {

  //displayed columns in table
  displayedColumns: string[] = ['name', 'value'];

  //displayed data
  dataSource: MatTableDataSource<PhylosRam> = new MatTableDataSource<PhylosRam>();

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public phylosModelState: PhylosModelStateService
  ) { 
    //get ram view
    this.dataSource.data = phylosModelState.phylosModel.RAM.getRamView();

    //subscribe to change events
    phylosModelState.phylosModel.RAM.RAMChange.subscribe(d => {
      //set the data only in degug mode
      if (this.phylosModelState.phylosModel.isDebugMode != PhylosDebugState.noDebug){
        this.dataSource.data = d;
      }
      
    });
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  /**
   * Updates the phylos ram model. 
   * @param elem Non Space Phylos RAM entry
   * @param value the new value
   */
  updateValue(elem: PhylosRam, value: string){
    if (elem.isSpace || isNaN(value as any)) return;
    //set the new ram value
    this.phylosModelState.phylosModel.RAM.set(elem.name,parseInt(value));
    //update until the next change detection run
    elem.value = parseInt(value);
  }

}
