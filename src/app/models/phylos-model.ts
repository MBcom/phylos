import { PhylosRegister } from './phylos-register';
import { of, Observable, from, Subject } from 'rxjs';
import { PhylosTransitionFunction } from './phylos-transition-function';
import { PhylosDebugState } from '../enums/phylos-debug-state.enum';
import { PhylosRamMap } from './phylos-ram-map';

export class PhylosModel {
        //register r0 ... r31
        public register: PhylosRegister[];

        //io register i0..i10
        public IORegister: PhylosRegister[];

        //source code
        public code: string = `# 
    # calculate scalar product <a,b> of the vectors a,b of length n,m
    #
    # precondition1: n=m
    # precondition2: a[i],b[i]>=0 for all 0<=i<n
    # precondition3: 
    #    vector a in memory beginning at address 0: [n, a[0],a[1], ..., a[n-1]]
    #    vector b in memory beginning at address 2^16: [m, b[0],b[1], ..., b[m-1]]
    #
    #
    # initialisation with example vectors a=(3,4) und b=(0,1) with length n=2,m=2
            addi r0 r1 2
            sto  r0 r1 0       # Hsp[0]=2
            addi r0 r1 3 
            sto  r0 r1 1       # Hsp[1]=3
            addi r0 r1 4
            sto  r0 r1 2       # Hsp[2]=4
            addi r0 r2 32767
            addi r2 r2 32767
            addi r2 r2 2
            addi r0 r1 2
            sto  r2 r1 0       # Hsp[2^16]=2
            addi r0 r1 0 
            sto  r2 r1 1       # Hsp[2^16+1]=0
            addi r0 r1 1
            sto  r2 r1 2       # Hsp[2^16+2]=1
    # check if <a,b> = 0, store result in r11
            addi r0  r3  0     # i=0
            ldd  r0 r1  0      # r1=n=Hsp[i]
            addi r2 r10 0      # j=2^16
            ldd  r10  r2 0     # r2=m=Hsp[j]
            addi r0  r11  0    # store result in r11, default: false
            bneq r1  r2  error # n/=m => result=-1 => error
    forB:   beq  r1 r3 false 
            addi r3 r3 1       # i++
            addi r10 r10 1     # j++
            ldd  r3 r4  0      # r4=Hsp[i]
            beq  r4 r0 forB    # next if a[i-1]==0
            ldd  r10  r5  0    # r5=Hsp[j]
            beq  r5 r0 forB    # next if b[i-1]==0
    
    true:   addi r0  r11 1     # r11=1 => true 
    
    false:                     # r11=0 => false
    end:    sysmove exc r0
            syscall
    
    error:  addi r0 r11 -1     # r11=-1 => error 
            jmp end
    `;

        //var for debug state
        //set of break points
        public breakpoints: Set<number> = new Set<number>();

        //line of debug step
        public currentDebugLine: number = 0;

        //mode
        public isDebugMode: PhylosDebugState = PhylosDebugState.noDebug;

        ///////////
        //list of errors
        public errorList: { lineNumber: number, errorMsg: string, errorType: 'error' | "warning" | "info" }[] = [];
        public errorListChange: Subject<{ lineNumber: number, errorMsg: string, errorType: 'error' | "warning" | "info" }[]> = new Subject();

        //web worker for syntax checking ad other cpu tasks
        private syntaxWorker: Worker;


        //Map for RAM
        public RAM: PhylosRamMap = new PhylosRamMap();

        //*************static vars ***********/

        //model transition functions
        public static modelTransitions: Map<string, PhylosTransitionFunction> = new Map<string, PhylosTransitionFunction>([
                ["syscall", (model) => {
                        return {
                                abort: true,
                                jump: false
                        };
                }],
                ["sysmove", (model) => {
                        //todo implement
                        return {
                                abort: false,
                                jump: false
                        };
                }],
                ["addi", (model) => {
                        //split command
                        var cmd = model.line.replace(/^\s+/, '').split(/\s{1,}/);
                        var r1: PhylosRegister = model.register[cmd[1].split('r')[1]];
                        var r2: PhylosRegister = model.register[cmd[2].split('r')[1]];
                        if (!r1) {
                                throw "Register " + cmd[1] + " not defined.";
                        }
                        if (!r2) {
                                throw "Register " + cmd[2] + " not defined.";
                        }
                        if (!r2.writable) {
                                throw "Register " + cmd[2] + " not writable.";
                        }
                        r2.value = r1.value + parseInt(cmd[3]);
                        return {
                                abort: false,
                                jump: false
                        };
                }],
                ["sto", (model) => {
                        //split command
                        var cmd = model.line.replace(/^\s+/, '').split(/\s{1,}/);
                        var r1: PhylosRegister = model.register[cmd[1].split('r')[1]];
                        var r2: PhylosRegister = model.register[cmd[2].split('r')[1]];
                        if (!r1) {
                                throw "Register " + cmd[1] + " not defined.";
                        }
                        if (!r2) {
                                throw "Register " + cmd[2] + " not defined.";
                        }
                        model.ram.set(r1.value + parseInt(cmd[3]), r2.value);
                        return {
                                abort: false,
                                jump: false
                        };
                }],
                ["ldd", (model) => {
                        //split command
                        var cmd = model.line.replace(/^\s+/, '').split(/\s{1,}/);
                        var r1: PhylosRegister = model.register[cmd[1].split('r')[1]];
                        var r2: PhylosRegister = model.register[cmd[2].split('r')[1]];
                        if (!r1) {
                                throw "Register " + cmd[1] + " not defined.";
                        }
                        if (!r2) {
                                throw "Register " + cmd[2] + " not defined.";
                        }
                        if (model.ram.has(r1.value + parseInt(cmd[3]))) {
                                r2.value = model.ram.get(r1.value + parseInt(cmd[3]));
                        } else {
                                r2.value = 0; //default value
                        }
                        return {
                                abort: false,
                                jump: false
                        };
                }],
                ["beq", (model) => {
                        //split command
                        var cmd = model.line.replace(/^\s+/, '').split(/\s{1,}/);
                        var r1: PhylosRegister = model.register[cmd[1].split('r')[1]];
                        var r2: PhylosRegister = model.register[cmd[2].split('r')[1]];
                        if (!r1) {
                                throw "Register " + cmd[1] + " not defined.";
                        }
                        if (!r2) {
                                throw "Register " + cmd[2] + " not defined.";
                        }

                        return {
                                abort: false,
                                jump: r1.value == r2.value,
                                jumpMark: cmd[3]
                        };
                }],
                ["bneq", (model) => {
                        //split command
                        var cmd = model.line.replace(/^\s+/, '').split(/\s{1,}/);
                        var r1: PhylosRegister = model.register[cmd[1].split('r')[1]];
                        var r2: PhylosRegister = model.register[cmd[2].split('r')[1]];
                        if (!r1) {
                                throw "Register " + cmd[1] + " not defined.";
                        }
                        if (!r2) {
                                throw "Register " + cmd[2] + " not defined.";
                        }

                        return {
                                abort: false,
                                jump: r1.value != r2.value,
                                jumpMark: cmd[3]
                        };
                }],
                ["jmp", (model) => {
                        //split command
                        var cmd = model.line.replace(/^\s+/, '').split(/\s{1,}/);

                        return {
                                abort: false,
                                jump: true,
                                jumpMark: cmd[1]
                        };
                }]
        ]);





        

        //commands
        public static syntaxCommands: { command: string, parameters: string }[] = [
                { command: "(addi|sto|ldd)", parameters: "r[0-9]+ +r[0-9]+ +\-?\\d+ *$" },
                { command: "(beq|bneq)", parameters: "r[0-9]+ +r[0-9] +(@jumpMark) *$" },
                { command: "jmp", parameters: "(@jumpMark) *$" },
                { command: "^( |\t)*$"/*empty command*/, parameters: "" },
                { command: "^ *[A-Za-z][A-Za-z0-9]*: *$"/*jump mark with no command*/, parameters: "" },
                { command: "syscall", parameters: "" },
                { command: "sysmove", parameters: "exc +r[0-9]+" }];

        /**
         * Class representing the phylos code model
         * @param syntaxWorker a optional web worker for syntax checking
         * @param defaults a string previously returned from toJson()
         */
        constructor(syntaxWorker?: Worker, defaults?: string) {
                //initialize register array
                this.register = [...Array(32).keys()].map(k => {
                        return {
                                name: 'r' + k,
                                value: 0,
                                writable: k != 0
                        }
                });

                //initialize io register array
                this.IORegister = [...Array(11).keys()].map(k => {
                        return {
                                name: 'I' + k,
                                value: 0,
                                writable: true
                        }
                });

                //syntax checking in another js thread
                this.syntaxWorker = syntaxWorker;
                if (syntaxWorker) {
                        this.syntaxWorker.onmessage = ({ data }) => {
                                //clear array
                                this.errorList.length = 0;
                                //push the new messages
                                this.errorList.push(...data);
                                //propagate event
                                this.errorListChange.next(data);
                        };

                        //on code changes run the syntax checker
                        setInterval(() => {
                                syntaxWorker.postMessage(this.code)
                        }, 500);
                }

                if(defaults){
                        try {
                             var def = JSON.parse(defaults);
                             this.register.length = 0;
                             this.register.push(...def.register);

                             this.IORegister.length = 0;
                             this.IORegister.push(...def.IORegister);
                             
                             this.breakpoints = new Set(def.breakpoints);

                             this.code = def.code;

                             this.RAM = new PhylosRamMap(def.RAM);
                        } catch (error) {
                                console.error("Could't instantiate phylos mode from string: "+defaults+"; "+error)
                        }
                }
        }

        /**
         * Encodes this instance as json
         */
        public toJson(): string {
                var ret: { [key: string]: any } = {};
                ret.code = this.code;

                ret.RAM = Array.from(this.RAM.entries());

                ret.register = this.register;

                ret.breakpoints = Array.from(this.breakpoints);

                ret.IORegister = this.IORegister;

                return JSON.stringify(ret);
        }
}
