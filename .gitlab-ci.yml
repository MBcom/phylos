# This file is a template, and might need editing before it works on your project.
# Official framework image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/node/tags/
image: node:latest

  
variables:
  # AUTO_DEVOPS_DOMAIN is the application deployment domain and should be set as a variable at the group or project level.
  AUTO_DEVOPS_DOMAIN: kube.informatik.uni-halle.de

  POSTGRES_ENABLED: "false" #set to true when postgres is needed
  POSTGRES_DB: $CI_ENVIRONMENT_SLUG

  APP_PORT: 80

  KUBERNETES_VERSION: 1.16.1
  HELM_VERSION: 2.15.0

  DOCKER_DRIVER: overlay2

  

# This folder is cached between builds
# http://docs.gitlab.com/ce/ci/yaml/README.html#cache
cache:
  paths:
    - node_modules/

stages:
- test
- build
- build-docker
- deploy

test:
  stage: test
  image: kube.informatik.uni-halle.de:5005/ng-cli-karma:latest
  script:
    - npm install
    - ./node_modules/.bin/ng test  --watch=false --progress=false --code-coverage

test-cypress-e2e:
      stage: test
      image: cypress/browsers:node12.4.0-chrome76
      script:
        - export CYPRESS_CACHE_FOLDER=`pwd`/cypress/cache/
        - npm install
        - npm run e2e-docker
      cache:
        paths:
        - .npm
        - cypress/cache/
        - node_modules/
        - ~/.cache
        key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
      artifacts:
        paths:
          - cypress/videos/
          - cypress/screenshots/
        expire_in: 1d
        when: always
        reports:
          junit:
          - results/TEST-*.xml
  

    
build:
    stage: build
    script:
    - npm install
    - ./node_modules/.bin/ng build --prod
    artifacts:
        paths:
        - dist/phylos

build-docs:
          stage: build
          image: python:3.8.2
          before_script:
            - pip install mkdocs-material
          script:
            - mkdir docs/img
            - cp src/favicon.ico docs/img/
            - mkdocs build
          cache:
            paths:
              - ~/.cache
          artifacts:
           paths:
            - site

build_docker:
    stage: build-docker
    image: docker:18-git
    services:
    - docker:18-dind
    variables:
        DOCKER_DRIVER: overlay2
    script:
    - setup_docker
    - build

production:
  stage: deploy
  image: alpine:latest
  script:
    - install_dependencies
    - download_chart
    - install_tiller
    - deploy
    - persist_environment_url
  environment:
    name: production
    url: https://ajdcd-phylos.$AUTO_DEVOPS_DOMAIN
  artifacts:
    paths: [environment_url.txt]
  only:
    refs:
      - master
    kubernetes: active
    
    
.auto_devops: &auto_devops |
  # Auto DevOps variables and functions
  [[ "$TRACE" ]] && set -x 
  export auto_database_url="Host=${CI_ENVIRONMENT_SLUG}-postgres;Database=${POSTGRES_DB};Username=${POSTGRES_USER};Password=${POSTGRES_PASSWORD}"
  export DATABASE_URL=${DATABASE_URL-$auto_database_url}
  export CI_APPLICATION_TAG="latest"
  export CI_CONTAINER_NAME=ci_job_${CI_JOB_ID}
  export CI_APPLICATION_REPOSITORY=kube.informatik.uni-halle.de:5005/ajdcd-${CI_PROJECT_NAME}-master-${CI_COMMIT_REF_SLUG}


  export TILLER_NAMESPACE=$KUBE_NAMESPACE
  # Extract "MAJOR.MINOR" from CI_SERVER_VERSION and generate "MAJOR-MINOR-stable" for Security Products
  export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')


  

  function get_replicas() {
    track="${1:-stable}"
    percentage="${2:-100}"

    env_track=$( echo $track | tr -s  '[:lower:]'  '[:upper:]' )
    env_slug=$( echo ${CI_ENVIRONMENT_SLUG//-/_} | tr -s  '[:lower:]'  '[:upper:]' )

    if [[ "$track" == "stable" ]] || [[ "$track" == "rollout" ]]; then
      # for stable track get number of replicas from `PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_slug}_REPLICAS
      if [[ -z "$new_replicas" ]]; then
        new_replicas=$REPLICAS
      fi
    else
      # for all tracks get number of replicas from `CANARY_PRODUCTION_REPLICAS`
      eval new_replicas=\$${env_track}_${env_slug}_REPLICAS
      if [[ -z "$new_replicas" ]]; then
        eval new_replicas=\${env_track}_REPLICAS
      fi
    fi

    replicas="${new_replicas:-1}"
    replicas="$(($replicas * $percentage / 100))"

    # always return at least one replicas
    if [[ $replicas -gt 0 ]]; then
      echo "$replicas"
    else
      echo 1
    fi
  }
 
  # Extracts variables prefixed with K8S_SECRET_
  # and creates a Kubernetes secret.
  #
  # e.g. If we have the following environment variables:
  #   K8S_SECRET_A=value1
  #   K8S_SECRET_B=multi\ word\ value
  #
  # Then we will create a secret with the following key-value pairs:
  #   data:
  #     A: dmFsdWUxCg==
  #     B: bXVsdGkgd29yZCB2YWx1ZQo=
  function create_application_secret() {
    track="${1-stable}"
    export APPLICATION_SECRET_NAME=$(application_secret_name "$track")

    bash -c '
      function k8s_prefixed_variables() {
        env | sed -n "s/^K8S_SECRET_\(.*\)$/\1/p"
      }

      kubectl create secret \
        -n "$KUBE_NAMESPACE" generic "$APPLICATION_SECRET_NAME" \
        --from-env-file <(k8s_prefixed_variables) -o yaml --dry-run |
        kubectl replace -n "$KUBE_NAMESPACE" --force -f -
    '
  }

  function deploy_name() {
    name="$CI_ENVIRONMENT_SLUG"
    track="${1-stable}"

    if [[ "$track" != "stable" ]]; then
      name="$name-$track"
    fi

    echo $name
  }

  function application_secret_name() {
    track="${1-stable}"
    name=$(deploy_name "$track")

    echo "${name}-secret"
  }
  
  function deploy() {
    track="${1-stable}"
    percentage="${2:-100}"
    name=$(deploy_name "$track")

    replicas="1"
    service_enabled="true"
    postgres_enabled="$POSTGRES_ENABLED"

    # if track is different than stable,
    # re-use all attached resources
    if [[ "$track" != "stable" ]]; then
      service_enabled="false"
      postgres_enabled="false"
    fi

    replicas=$(get_replicas "$track" "$percentage")

    if [[ "$CI_PROJECT_VISIBILITY" != "public" ]]; then
      secret_name='gitlab-registry'
    else
      secret_name=''
    fi

    create_application_secret "$track"

   
      echo "Deploying new release..."
      helm upgrade --install \
        --wait \
        --set releaseOverride="$CI_ENVIRONMENT_SLUG" \
        --set service.url="$CI_ENVIRONMENT_URL" \
        --set replicaCount="$replicas" \
        --set service.externalPort=${APP_PORT} \
        --set ingress.tls.secretName="kube-cert" \
        --set image.repository="$CI_APPLICATION_REPOSITORY" \
        --set postgresql.enabled="false" \
        --set service.internalPort=${APP_PORT} \
        --set image.tag="$CI_APPLICATION_TAG" \
        --set image.pullPolicy=Always \
        --set image.secrets[0].name="$secret_name" \
        --set application.commitSha="${CI_COMMIT_SHORT_SHA}" \
        --set podAnnotations.commitSha="${CI_COMMIT_SHORT_SHA}" \
        --namespace="$KUBE_NAMESPACE" \
        --force \
        "$name" \
        chart/

    kubectl rollout status -n "$KUBE_NAMESPACE" -w "deployment/$name"
  }

  function scale() {
    track="${1-stable}"
    percentage="${2-100}"
    name=$(deploy_name "$track")

    replicas=$(get_replicas "$track" "$percentage")

    if [[ -n "$(helm ls -q "^$name$")" ]]; then
      helm upgrade --reuse-values \
        --wait \
        --set replicaCount="$replicas" \
        --namespace="$KUBE_NAMESPACE" \
        "$name" \
        chart/
    fi
  }

  function install_dependencies() {
    apk add -U openssl curl tar gzip bash ca-certificates git
    curl -L -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub || echo 1
    curl -L -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.28-r0/glibc-2.28-r0.apk || echo 1
    apk add glibc-2.28-r0.apk
    rm glibc-2.28-r0.apk

    curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
    mv linux-amd64/helm /usr/bin/
    mv linux-amd64/tiller /usr/bin/
    helm version --client
    tiller -version

    curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    chmod +x /usr/bin/kubectl
    kubectl version --client
  }

  function setup_docker() {
    if ! docker info &>/dev/null; then
      if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
      fi
    fi
  }

  function setup_test_db() {
    if [ -z ${KUBERNETES_PORT+x} ]; then
      DB_HOST=postgres
    else
      DB_HOST=localhost
    fi
    export DATABASE_URL="postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${DB_HOST}:5432/${POSTGRES_DB}"
  }

  function download_chart() {
    if [[ ! -d chart ]]; then
      auto_chart="informatik-repo/auto-deploy-app"
      auto_chart_name=$(basename $auto_chart)
      auto_chart_name=${auto_chart_name%.tgz}
    else
      auto_chart="chart"
      auto_chart_name="chart"
    fi

    helm init --client-only
    helm repo add informatik-repo https://helm-charts.kube.informatik.uni-halle.de
    if [[ ! -d "$auto_chart" ]]; then
      helm fetch ${auto_chart} --untar
    fi
    if [ "$auto_chart_name" != "chart" ]; then
      mv ${auto_chart_name} chart
    fi

    helm dependency update chart/
    helm dependency build chart/
  }

  function ensure_namespace() { # namespace is creted over Serviceportal
    #check rights for Namespace
    kubectl describe namespace "$KUBE_NAMESPACE"
  }

  function check_kube_domain() {
    if [ -z ${AUTO_DEVOPS_DOMAIN+x} ]; then
      echo "In order to deploy or use Review Apps, AUTO_DEVOPS_DOMAIN variable must be set"
      echo "You can do it in Auto DevOps project settings or defining a secret variable at group or project level"
      echo "You can also manually add it in .gitlab-ci.yml"
      false
    else
      true
    fi
  }

  function build() {

    if [[ -f Dockerfile ]]; then
      echo "Building Dockerfile-based application..."
      docker pull $CI_APPLICATION_REPOSITORY:latest || true
      docker build \
        --build-arg HTTP_PROXY="$HTTP_PROXY" \
        --build-arg http_proxy="$http_proxy" \
        --build-arg HTTPS_PROXY="$HTTPS_PROXY" \
        --build-arg https_proxy="$https_proxy" \
        --build-arg FTP_PROXY="$FTP_PROXY" \
        --build-arg ftp_proxy="$ftp_proxy" \
        --build-arg NO_PROXY="$NO_PROXY" \
        --build-arg no_proxy="$no_proxy" \
        --cache-from $CI_APPLICATION_REPOSITORY:latest \
        -t "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" .
    else
      echo "Building Heroku-based application using gliderlabs/herokuish docker image..."
      docker run -i \
        -e BUILDPACK_URL \
        -e HTTP_PROXY \
        -e http_proxy \
        -e HTTPS_PROXY \
        -e https_proxy \
        -e FTP_PROXY \
        -e ftp_proxy \
        -e NO_PROXY \
        -e no_proxy \
        --name="$CI_CONTAINER_NAME" -v "$(pwd):/tmp/app:ro" gliderlabs/herokuish /bin/herokuish buildpack build
      docker commit "$CI_CONTAINER_NAME" "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
      docker rm "$CI_CONTAINER_NAME" >/dev/null
      echo ""

      echo "Configuring $CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG docker image..."
      docker create --expose 5000 --env PORT=5000 --name="$CI_CONTAINER_NAME" "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG" /bin/herokuish procfile start web
      docker commit "$CI_CONTAINER_NAME" "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
      docker rm "$CI_CONTAINER_NAME" >/dev/null
      echo ""
    fi

    echo "Pushing to GitLab Container Registry..."
    docker push "$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
    echo "Fertig mit pushen."
  }

  function install_tiller() {
    echo "Checking Tiller..."

    export HELM_HOST="localhost:44134"
    tiller -listen ${HELM_HOST} -alsologtostderr > /dev/null 2>&1 &
    echo "Tiller is listening on ${HELM_HOST}"

    if ! helm version --debug; then
      echo "Failed to init Tiller."
      return 1
    fi
    echo ""
  }
  

  function persist_environment_url() {
      echo $CI_ENVIRONMENT_URL > environment_url.txt
  }

  function delete() {
    track="${1-stable}"
    name=$(deploy_name "$track")

    if [[ -n "$(helm ls -q "^$name$")" ]]; then
      helm delete --purge "$name"
    fi

    secret_name=$(application_secret_name "$track")
    kubectl delete secret --ignore-not-found -n "$KUBE_NAMESPACE" "$secret_name"
  }

before_script:
  - *auto_devops


