import { Injectable } from '@angular/core';
import { MultiWindowService } from 'ngx-golden-layout';
import { Observable, Subject, of } from 'rxjs';
import { PhylosTheme } from '../enums/phylos-theme.enum';
import { PhylosModel } from '../models/phylos-model';

/**
 * Service to hold Phylos state
 */

//make work this service across windows
@MultiWindowService<PhylosModelStateService>('PhylosModelStateService')
@Injectable()
export class PhylosModelStateService {
  //theme status
  public currentTheme: PhylosTheme = PhylosTheme["vs-dark"];
  public currentThemeChange: Subject<PhylosTheme> = new Subject<PhylosTheme>();

  //login status
  public isLoggedIn: boolean = sessionStorage.getItem('loggedIn') == '1';


  //phylos Model
  public phylosModel: PhylosModel;

  //undo redo requests - true for undo, false for redo
  public undoRedoRequested: Subject<boolean> = new Subject<boolean>();
  



  constructor() {

    //init webworker for syntax checking
    var a;
    if (typeof Worker !== 'undefined') {
      // Create a new
      a = new Worker('../webworker/syntax-checker.worker', { type: 'module' });
    } else {
      // Web workers are not supported in this environment.
    }

    //init phylos model
    if (localStorage.getItem('phylos_model')){
      this.phylosModel = new PhylosModel(a,localStorage.getItem('phylos_model'));
    }else{
      this.phylosModel = new PhylosModel(a);
    }
    

    //save model on page leave to local storage
    window.addEventListener("beforeunload", () =>{
      if (this.isLoggedIn){
        localStorage.setItem("phylos_model",this.phylosModel.toJson());
        localStorage.setItem('phylos_theme',this.currentTheme+"");
      }
    });

    if (localStorage.getItem('phylos_theme')){
      this.SetTheme(parseInt(localStorage.getItem('phylos_theme')));
    }
  }

  /**
   * Sets a new theme for the phylos.
   * All theme depended components must listen on currentTheme
   * @param newTheme 
   */
  public SetTheme(newTheme: PhylosTheme): void {
    this.currentTheme = newTheme;
    switch (newTheme) {
      case PhylosTheme["vs-dark"]:
        $('body').removeClass('pyhlos-theme-light').addClass('pyhlos-theme-dark');
        break;
      default:
        $('body').addClass('pyhlos-theme-light').removeClass('pyhlos-theme-dark');
        break;
    }
    this.currentThemeChange.next(newTheme);
  }

  /**
   * Logs user in
   * @param username 
   * @param password 
   * @returns true on success, false otherwise
   */
  public Login(username: string, password: string): boolean {
    //todo implement login logic
    this.isLoggedIn = true;
    sessionStorage.setItem('loggedIn', '1');
    return true;
  }

  /**
   * Logs user out.
   */
  public Logout(): void {
    this.isLoggedIn = false;
    localStorage.removeItem('phylos_layout');
    localStorage.removeItem('phylos_model');
    sessionStorage.removeItem('loggedIn');
    location.reload();
    //todo implement logout logic
  }


}
