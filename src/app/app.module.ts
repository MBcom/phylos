import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//editor imports
import { MonacoEditorModule, NgxMonacoEditorConfig } from 'ngx-monaco-editor';


//golden Layout imports
import { GoldenLayoutModule, ComponentType, MultiWindowInit } from 'ngx-golden-layout';
import * as $ from 'jquery';

//components
import { EditorComponent } from './components/editor/editor.component';
import { RegisterComponent } from './components/register/register.component';
import { RomComponent } from './components/rom/rom.component';
import { RamComponent } from './components/ram/ram.component';
import { ErrorListComponent } from './components/error-list/error-list.component';
import { LoginComponent } from './components/login/login.component';

//material module
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import {MatTableModule} from '@angular/material/table'; 
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox'; 
import {MatRadioModule} from '@angular/material/radio'; 
import {MatDividerModule} from '@angular/material/divider'; 

//accessiblity - aria
import {A11yModule} from '@angular/cdk/a11y';

//service managing the phylos model - singleton for all instances of browser windows
import { PhylosModelStateService } from './services/phylos-model-state.service';

import { KeyboardTableNavigationDirective } from './directives/keyboard-table-navigation.directive';
import { IoRegisterComponent } from './components/io-register/io-register.component';


// It is required to have JQuery as global in the window object.
window['$'] = $;

// Define all component types known at compile time to the golden-layout binding.
// It's possible to modify these at runtime using the `ComponentRegistryService`
const componentTypes: ComponentType[] = [{
  name: 'EditorComponent',
  type: EditorComponent,
},
{
  name: 'ErrorListComponent',
  type: ErrorListComponent,
},
{
  name: 'RamComponent',
  type: RamComponent,
},
{
  name: 'RomComponent',
  type: RomComponent,
},
{
  name: 'RegisterComponent',
  type: RegisterComponent,
},
{
  name: 'IoRegisterComponent',
  type: IoRegisterComponent,
}
];


//define imports
export const imports: any[] = [
  BrowserModule,
  BrowserAnimationsModule,
  FormsModule,
  MonacoEditorModule.forRoot(),
  GoldenLayoutModule.forRoot(componentTypes),
  MatButtonModule,
  MatTooltipModule,
  MatIconModule,
  MatBadgeModule,
  MatMenuModule,
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatCheckboxModule,
  MatRadioModule,
  MatDividerModule,
  A11yModule
];

export const declarations = [
  AppComponent,
  EditorComponent,
  RegisterComponent,
  RomComponent,
  RamComponent,
  ErrorListComponent,
  LoginComponent,
  KeyboardTableNavigationDirective,
  IoRegisterComponent
];


@NgModule({
  declarations: declarations,
  imports: imports,
  providers: [
    PhylosModelStateService
  ],
  bootstrap: [AppComponent],

  //under entryComponents list all the components that should be rendered within golden layout.
  entryComponents: [
    EditorComponent,
    RegisterComponent,
    RomComponent,
    RamComponent,
    ErrorListComponent,
    IoRegisterComponent
  ]
})
export class AppModule { }
