import { Component, OnInit, ViewChild } from '@angular/core';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';
import { of } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { Sort, MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-error-list',
  templateUrl: './error-list.component.html',
  styleUrls: ['./error-list.component.scss']
})
export class ErrorListComponent implements OnInit {

  //displayed columns in table
  displayedColumns: string[] = [ 'lineNumber','errorType','errorMsg'];

  //data source
  dataSource = new MatTableDataSource(this.phylosModelState.phylosModel.errorList);

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public phylosModelState: PhylosModelStateService
  ) { 
    //update data on change
    phylosModelState.phylosModel.errorListChange.subscribe(e => {
      this.dataSource.data = e;
    })
  }

  ngOnInit() {
    //set problem counter - because it is not in agular environment, we use the browser api here
    this.phylosModelState.phylosModel.errorListChange.subscribe(e => {  
      window.document.getElementById('phylos_problemCounter').innerText = e.length+"";
    })

    this.dataSource.sort = this.sort;
  }

}
