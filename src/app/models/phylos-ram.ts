export interface PhylosRam {
    name?: number; //RAM address, here are only values if space = false
    value?: number; //RAM value, here are only values if space = false
    isSpace: boolean; //determines if there is a space between the preceeding item and the following address entry
}
