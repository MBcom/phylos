export interface PhylosInstruction {
    address: number;
    opcode: number;
    command: string;
}
