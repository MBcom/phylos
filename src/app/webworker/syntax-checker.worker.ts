import { PhylosModel } from "../models/phylos-model";

/// <reference lib="webworker" />
enum MarkerSeverity {
  Hint = 1,
  Info = 2,
  Warning = 4,
  Error = 8
};

var text: string = "";

addEventListener('message', ({ data }) => {
  //event when new data is recieved
  if (data != text){   
    text = data;
    try {
      postMessage(checkSyntax(data),null);
    } catch (error) {
      console.error(error);
    }
  }
});

function checkSyntax(text: string) {
  var ret:{ lineNumber: number, errorMsg: string, errorType: 'error' | "warning" | "info" }[] = [];

  var jumpMarks = [];
  var regexMatch;
  var jumpMarkRegex = /^ *([A-Za-z][A-Za-z0-9]*):/gm;
  do {
    regexMatch = jumpMarkRegex.exec(text);
    if (regexMatch){
      jumpMarks.push(regexMatch[1]);
    }
  } while (regexMatch);

  text.split(/\r?\n/).forEach((t, i) => {
    var len = t.length;

    //kommentare ersetzen
    t = t.split(/#.*#/).join('');

    //one line comments
    t = t.replace(/#.*/, '');

    //remove tabs
    t = t.split(/\t/).join(' ');

    if (/^\s*$/.test(t)) return;

    var errMsg: string = "";
    var hasError: boolean = true;

    for (let index = 0; index < PhylosModel.syntaxCommands.length; index++) {
      const r = PhylosModel.syntaxCommands[index];
      
      if (new RegExp(r.command+'( |$)').test(t)) {
        //check parameters
        if (new RegExp(r.command + "( +|$)" + r.parameters.replace('@jumpMark', '(' + jumpMarks.join('|') + ')')).test(t)) {
          //command which is matching was found
          hasError = false;
          break;
        }
        //command didn't match
        if (r.parameters.includes('@jumpMark') && (new RegExp(r.command + " +" + r.parameters.replace('@jumpMark', '([A-Za-z][A-Za-z0-9]*)')).test(t))) {
          //jump mark not found
          errMsg = "Error: Jump mark not found.";
        } else {
          errMsg = "Error: Paramter did not match."
        }

        break;
      }
    }

    if (hasError) {
      if (errMsg == "") {
        errMsg = `Error: unrecognized command '${t}'`;
      }

      //add error mark to response
      ret.push({
        lineNumber: i + 1,
        errorMsg: errMsg,
        errorType: "error"
      });
    }


  });

  return ret;
}