import { Directive, ViewChildren, QueryList, HostListener, ContentChildren } from '@angular/core';
import { MatInput } from '@angular/material/input';

/**
 * Directive for enabling keyboard navigation with arrow keys in our table views
 */
@Directive({
  selector: '[appKeyboardTableNavigation]'
})
export class KeyboardTableNavigationDirective {

  
  //input elements
  @ContentChildren(MatInput, {descendants: true})
  inputs: QueryList<MatInput>

  
  constructor() { }

  /**
   * Function for keyboard navigation table
   */
  @HostListener('keydown', ['$event'])
  onKeyDown(ev: KeyboardEvent): void {
    if((ev.target as HTMLElement).tagName != 'INPUT') return;
    var a = this.inputs.toArray();
    for (var i = 0; i < a.length; i++) {
      let e = a[i];
      if (e.focused) {

        if (ev.keyCode == 40 && i + 1 < a.length) {
          a[i + 1].focus();
        } 
        if (ev.keyCode == 38 && i >= 1) {
          a[i - 1].focus();
        }
        break;
      }
    }
  }
}
