import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorListComponent } from './error-list.component';
import { imports, declarations } from 'src/app/app.module';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';

describe('ErrorListComponent', () => {
  let component: ErrorListComponent;
  let fixture: ComponentFixture<ErrorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: declarations,
      imports: imports,
      providers: [
        PhylosModelStateService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
