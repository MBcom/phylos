import { Subject } from 'rxjs';
import { PhylosRam } from './phylos-ram';

export class PhylosRamMap extends Map<number, number>{
    public RAMChange: Subject<PhylosRam[]> = new Subject<PhylosRam[]>();

    public set(key: number, value: number): any {
        super.set(key, value);
        //emit event
        this.RAMChange.next(this.getRamView());
        return this;
    }

    public delete(key:number): any{
        super.delete(key);
        //emit event
        this.RAMChange.next(this.getRamView());
        return this;
    }

    /**
         * Transforms the RAM modell to a human redeable form.
         * Includes space information, between free RAM values
         */
    public getRamView(): PhylosRam[] {
        var ret: PhylosRam[] = [];


        //display first 10 registers anytime
        for (let i = 0; i < 10; i++) {
            if (this.has(i)) {
                ret.push({
                    isSpace: false,
                    name: i,
                    value: this.get(i)
                });
            } else {
                ret.push({
                    isSpace: false,
                    name: i,
                    value: 0
                });
            }
        }

        var lastAdded: number = 9;
        //add the others
        for (let [k, v] of this) {
            //continue if already added above
            if (k < 10) continue;

            //add entry
            if (lastAdded + 1 != k) {
                //added space between
                ret.push({
                    isSpace: true,
                    value: k - lastAdded - 1
                })
            }

            //set value
            ret.push({
                isSpace: false,
                name: k,
                value: v
            })
            //set last added pointer
            lastAdded = k;
        }

        //add space at the end if required
        if (ret[ret.length - 1].value != Number.MAX_VALUE) {
            ret.push({
                isSpace: true,
                value: Number.MAX_VALUE - lastAdded - 1
            })
        }
        return ret;
    }
}
