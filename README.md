[![Logo](src/assets/logo.svg)](src/assets/logo.svg)
## PHYLOS is as web based assembly code editor and debugger.

See a demo here: https://ajdcd-phylos.kube.informatik.uni-halle.de

# Development
For the commands you need to have the Angular CLI globally installed. (`npm install -g @angular/cli`), otherwise you have to replace `ng` with `./node_modules/.bin/ng`.

To get started you have to run `npm install` once.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `./node_modules/.bin/cypress open` to execute the end-to-end tests.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Project overview
The documentation is placed in the `/docs/` folder as Markdown document.
The source code is placed under `/src`.
The syntax checking is done in a seperate web worker under `/src/app/webworker/syntax-checker.worker.ts`, the code highlighting in `/src/app/components/editor/editor.component.ts`.
There is a service `/src/app/services/phylos-model-state.service.ts` which is holding the phylos model defined in `/src/app/model/phylos-model.ts`.
This is service is available in the main browser window and the popouts. The service is a singleton and manages other main global parts like logging in too.

### Add a new command
For adding a new command you have to do the following:
* add a model transition/ transform function for your new command to  `src/app/models/phylos-model.ts > modelTransitions` e.g.:  
```typescript
["your_command", (model) => {
                        //split command
                        var cmd = model.line.replace(/^\s+/, '').split(/\s{1,}/);
                        var r1: PhylosRegister = model.register[cmd[1].split('r')[1]];
                        var r2: PhylosRegister = model.register[cmd[2].split('r')[1]];
                        if (!r1) {
                                throw "Register " + cmd[1] + " not defined.";
                        }
                        if (!r2) {
                                throw "Register " + cmd[2] + " not defined.";
                        }
                        if (!r2.writable) {
                                throw "Register " + cmd[2] + " not writable.";
                        }
                        r2.value = r1.value + parseInt(cmd[3]);
                        return {
                                abort: false,
                                jump: false
                        };
                }]
```  
  
  The parameter `model` contains a reference to the line to run (without jump marks at the beginning and comments) and the ByRef references to register and RAM. It must return if the debugger should abort or jump. 
  The debugger starts only if the syntax checker did not report errors, so runtime errors should not occur.
  But you still have to do error handling in your model transition functions.
  Your transition function will only be called if the command `your_command` was detected in current line.
* you have to add a regular expression for your command to  `src/app/models/phylos-model.ts > syntaxCommands`, this is used for the syntax checker. In the parameters Regex you can use the placeholder `@jumpMark` to check if there is a defined jump mark.
* for correct syntax highlithing you have to add your new command to the monarch tokenizer of our monaco editor. Here you have to go in `src/app/components/editor/editor.component.ts > editorInit() > monaco.languages.setMonarchTokensProvider`. For further reference see https://microsoft.github.io/monaco-editor/monarch.html
* if you want that your command can be autocomplete, you have to go in `src/app/components/editor/editor.component.ts > editorInit() > monaco.languages.registerCompletionItemProvider`. For further reference see https://microsoft.github.io/monaco-editor/playground.html#extending-language-services-completion-provider-example
* if you want that parts of your command have a special hover message e.g. in debug mode for showing register contents, you have to go in `src/app/components/editor/editor.component.ts > editorInit() > monaco.languages.registerHoverProvider`. For further reference see https://microsoft.github.io/monaco-editor/playground.html#extending-language-services-hover-provider-example
* if you want a special formatting for your new command when the user formats his document you have to go in `src/app/components/editor/editor.component.ts > editorInit() > monaco.languages.registerDocumentFormattingEditProvider`.

### Phylos Model
```typescript
{
    register: {name: string; value: number; writable: boolean}[]; //ordered by name
    IORegister: {name: string; value: number; writable: boolean}[]; //ordered by name
    code: string;
    breakpoints: Set<number>;
    currentDebugLine: number;
    isDebugMode: 0|1|2;
    errorList: { lineNumber: number, errorMsg: string, errorType: 'error' | "warning" | "info" }[];
    RAM: Map<number,number>;
}
```
For further reference see `src/app/models/phylos-model.ts`.

# Links
* low fidelity prototype: https://users.informatik.uni-halle.de/ajdcd/phylos/low_fid_1.jpg , https://users.informatik.uni-halle.de/ajdcd/phylos/low_fid_2.jpg
* user story https://users.informatik.uni-halle.de/ajdcd/phylos/user_story.jpg
