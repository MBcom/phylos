import { Component, OnInit, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';
import { MatInput } from '@angular/material/input';
import { PhylosRegister } from 'src/app/models/phylos-register';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  //displayed columns in table
  displayedColumns: string[] = ['name', 'value'];

  //displayed data
  dataSource = new MatTableDataSource<PhylosRegister>(this.phylosModelState.phylosModel.register);

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public phylosModelState: PhylosModelStateService
  ) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  
}
