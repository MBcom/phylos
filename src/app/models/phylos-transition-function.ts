import { PhylosRegister } from './phylos-register';

export interface PhylosTransitionFunction {
    //pass data es object, for passing it as reference
    (model:
        {
            line: string /* without comments*/,
            register: PhylosRegister[], /* ref to register state */
            ioregister: PhylosRegister[], /* ref to io register state */
            ram: Map<number, number> /* ref to ram state */
        }
    ): {
        jump: boolean; //true if debugger should jump to jump mark specified under 'jumpMark'
        jumpMark?: string; //the jump mark to jump to, if jump = true
        abort: boolean; //the debugger should stop execution
    }
}
