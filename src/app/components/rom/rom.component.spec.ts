import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RomComponent } from './rom.component';
import { imports, declarations } from 'src/app/app.module';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';

describe('RomComponent', () => {
  let component: RomComponent;
  let fixture: ComponentFixture<RomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: declarations,
      imports: imports,
      providers: [
        PhylosModelStateService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create instruction set', () => {
    component.phylosModelState.phylosModel.code = `
    # comment
    addi # inline comment# r0  r1 1
    abc:   subbi r0    r1    0 # huhuhaud
    abc:   subbi r0    r1    0 # huhuhaud
    `;
    component.generateInstructionSet();

    expect(component.dataSource.data).toEqual([
      {
        address: 0,
        command: "NOP",
        opcode: 0
      },
      {
        address: 1,
        command: "NOP",
        opcode: 0
      },
      {
        address: 2,
        command: "addi r0 r1 1",
        opcode: 2989032
      },
      {
        address: 3,
        command: "subbi r0 r1 0",
        opcode: 109787783 
      },
      {
        address: 4,
        command: "subbi r0 r1 0",
        opcode: 109787783
      },
      {
        address: 5,
        command: "NOP",
        opcode: 0
      }
    ])
  })
});
