describe('user-story', () => {
    it('user-story', () => {
        localStorage.clear();
        sessionStorage.clear();

        cy.visit('http://localhost:4200');

        cy.log('login in');

        cy.get('input[name="username"]').type('test');
        cy.get('input[name="password"]').type('test{enter}');

        cy.log('should be logged in');
        cy.get('nav li:first button:first').click();
        cy.get('button.mat-menu-item[role="menuitem"]').contains('New').click();

        cy.log('write code');
        cy.get('.editor-container').click().type('addi r0 r1 5{enter}test');
        cy.wait(500);
        cy.log('test error logging');
        cy.get('app-error-list mat-cell.cdk-column-errorMsg').contains("Error: unrecognized command 'test'");
        cy.get('.editor-container').type('{backspace}{backspace}{backspace}{backspace}#test');
        cy.get('app-error-list mat-cell.cdk-column-errorMsg').should('have.length',0);

        cy.log('write rest of programm');
        cy.get('.editor-container').type('{enter}addi r2 r3 5{enter}jmp huhu # comment{enter}addi r3 r4 6{enter}huhu:{pageup}');

        cy.log('set breakpoint');
        cy.get('ngx-monaco-editor .margin-view-overlays div:nth-child(5)').click().get('.break-point');
        cy.get('.editor-container').type('{pageup}');
        cy.wait(500);
        cy.get('ngx-monaco-editor .margin-view-overlays div:nth-child(3)').click().get('.break-point');

        cy.log('begin debugging');
        cy.get('app-editor nav button').click();
        cy.get('app-editor .debug-line').should('have.length',1);
        cy.get('app-register mat-row:nth-child(3) mat-cell:nth-child(2)').contains('0');
        cy.get('.lm_tab[title="ROM"]').click();
        cy.get('app-rom p').contains('0'); 
        cy.get('app-editor nav button[mattooltip="next Step (F10)"]').click();
        cy.get('app-rom p').contains('10');
        //test ob comment translated to NOP
        cy.get('app-rom mat-row:nth-child(3) mat-cell:nth-child(3)').contains('NOP');
        //check rest of command transforming
        cy.get('app-rom mat-row:nth-child(5) mat-cell:nth-child(3)').contains('jmp huhu');
        cy.get('app-rom mat-row:nth-child(7) mat-cell:nth-child(3)').contains('NOP');
        cy.get('.lm_tab[title="Register"]').click();
        cy.get('app-register mat-row:nth-child(3) mat-cell:nth-child(2)').contains('5');
        //resume
        cy.get('app-editor nav button').contains('Resume').click();
        cy.get('app-editor nav button').should('have.length',1);
        cy.get('app-register mat-row:nth-child(4) mat-cell:nth-child(2) input').focus().type('{downarrow}');
        cy.get('app-register mat-row:nth-child(5) mat-cell:nth-child(2) input').should('have.focus').should('have.value','5');

        cy.log('test error checking again');
        cy.get('.editor-container').type('{enter}addi huhu r5 0');
        cy.get('app-error-list mat-cell.cdk-column-errorMsg').should('have.length',1);
        cy.get('app-error-list mat-cell.cdk-column-errorMsg').contains("Error: Paramter did not match.");
        cy.get('app-error-list mat-cell.cdk-column-lineNumber').contains("7");
    })
})