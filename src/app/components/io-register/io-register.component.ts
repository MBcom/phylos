import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PhylosRegister } from 'src/app/models/phylos-register';
import { MatSort } from '@angular/material/sort';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';

@Component({
  selector: 'app-io-register',
  templateUrl: './io-register.component.html',
  styleUrls: ['./io-register.component.scss']
})
export class IoRegisterComponent implements OnInit {

  //displayed columns in table
  displayedColumns: string[] = ['name', 'value'];

  //displayed data
  dataSource = new MatTableDataSource<PhylosRegister>(this.phylosModelState.phylosModel.IORegister);

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public phylosModelState: PhylosModelStateService
  ) { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

}
