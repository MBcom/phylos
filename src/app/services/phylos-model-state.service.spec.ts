import { TestBed } from '@angular/core/testing';

import { PhylosModelStateService } from './phylos-model-state.service';
import { declarations, imports } from '../app.module';

describe('PhylosModelStateService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: declarations,
    imports: imports,
    providers: [
      PhylosModelStateService
    ]
  }));

  it('should be created', () => {
    const service: PhylosModelStateService = TestBed.get(PhylosModelStateService);
    expect(service).toBeTruthy();
  });
});
