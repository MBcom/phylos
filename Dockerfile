# This file is a template, and might need editing before it works on your project.
FROM httpd:alpine

COPY ./dist/phylos/ /usr/local/apache2/htdocs/

# copy docs created with mkdocs to container
RUN mkdir /usr/local/apache2/htdocs/docs
COPY ./site/ /usr/local/apache2/htdocs/docs/
