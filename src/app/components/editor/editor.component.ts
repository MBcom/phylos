import { Component, OnInit, Inject } from '@angular/core';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';
import { PhylosTheme } from 'src/app/enums/phylos-theme.enum';
import { of } from 'rxjs';
import { GoldenLayoutContainer } from 'ngx-golden-layout';
import * as GoldenLayout from 'golden-layout';
import { PhylosDebugState } from 'src/app/enums/phylos-debug-state.enum';
import { PhylosModel } from 'src/app/models/phylos-model';

/**
 * Component for managing the Code Editor incl. debugging
 */
@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {
  //editor theming
  editorOptions: monaco.editor.IEditorConstructionOptions = { theme: 'vs-dark', language: 'phylos', lineNumbers: "on", glyphMargin: true };

  //editor access
  editor: monaco.editor.IStandaloneCodeEditor;

  //var for debug mode
  codeLines: string[] = [];
  jumpMarks: Map<string, number> = new Map();


  //decorations for monaco editor, breakpoints and line marks
  decorations: monaco.editor.IModelDeltaDecoration[] = [];
  decorationReference: string[] = [];

  constructor(
    public phylosModelState: PhylosModelStateService,
    @Inject(GoldenLayoutContainer) private container: GoldenLayout.Container
  ) {
    //important hack, window.require is else not loaded because of the ngx-golden-layout lib
    (window as any).define.amd = undefined;
  }

  ngOnInit() {
  }

  /**
   * Sets the editor theme
   * @param theme 
   */
  private setEditorTheme(theme: PhylosTheme): void {
    switch (theme) {
      case PhylosTheme["vs-dark"]:
        this.editorOptions.theme = "vs-dark";
        break;
      default:
        this.editorOptions.theme = "vs";
        break;
    }
    this.editor.updateOptions(this.editorOptions);
    (window as any).monaco.editor.setTheme(this.editorOptions.theme);
  }


  /**
   * Called on editor init
   * @param editor monaco editor object
   */
  editorInit(ev: monaco.editor.IStandaloneCodeEditor) {
    this.editor = ev;

    // Here you can access editor instance
    var monaco = (window as any).monaco;

    //register phylos language
    monaco.languages.register({ id: 'phylos' });

    //register tokenizer for syntax highl
    monaco.languages.setMonarchTokensProvider('phylos', {
      // Set defaultToken to invalid to see what you do not tokenize yet
      defaultToken: 'invalid',

      registers: this.phylosModelState.phylosModel.register.map(r => r.name),

      typeKeywords: [
      ],

      operators: [
        'addi', 'sto', 'syscall', 'sysmove', 'beq', 'bneq', 'ldd'
      ],

      // we include these common regular expressions
      symbols: /[]+/,

      // C# style strings
      escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

      // The main tokenizer for our languages
      tokenizer: {
        root: [
          //comments
          [/#.*?#/, 'comment'],
          [/#.*?$/, 'comment'],

          // identifiers and keywords

          [/^( |\t)*(\w+):/, 'keyword'],  // to show class names nicely

          // numbers
          [/\d*\.\d+([eE][\-+]?\d+)?/, 'number.float'],
          [/0[xX][0-9a-fA-F]+/, 'number.hex'],
          [/\-?\d+/, 'number'],

          //operatoren und register
          [/[a-zA-Z0-9]+/, {
            cases: {
              '@operators': 'operator',
              '@registers': 'constant',
              '@default': 'identifier'
            }
          }],

          //bezeichner


          // whitespace
          { include: '@whitespace' },



          // delimiter: after number because of .\d floats
          [/[\n]/, 'delimiter'],

          // strings
          [/"([^"\\]|\\.)*$/, 'string.invalid'],  // non-teminated string
          [/"/, { token: 'string.quote', bracket: '@open', next: '@string' }],

          // characters
          [/'[^\\']'/, 'string'],
          [/(')(@escapes)(')/, ['string', 'string.escape', 'string']],
          [/'/, 'string.invalid']
        ],

        // comment: [
        //   [/[^\/*]+/, 'comment'],
        //   [/\/\*/, 'comment', '@push'],    // nested comment
        //   ["\\*/", 'comment', '@pop'],
        //   [/[\/*]/, 'comment']
        // ],

        string: [
          [/[^\\"]+/, 'string'],
          [/@escapes/, 'string.escape'],
          [/\\./, 'string.escape.invalid'],
          [/"/, { token: 'string.quote', bracket: '@close', next: '@pop' }]
        ],

        whitespace: [
          [/[ \t\r\n]+/, 'white'],
          // [/\/\*/, 'comment', '@comment'],
          // [/\/\/.*$/, 'comment'],
        ],
      }
    } as any);

    // Register a completion item provider for the new language
    monaco.languages.registerCompletionItemProvider('phylos', {
      provideCompletionItems: (model, position) => {
        // find out if we are completing a property in the 'dependencies' object.
        var textUntilPosition = model.getValueInRange({ startLineNumber: position.lineNumber, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column });
        var match = textUntilPosition.match(/#/);
        if (match) {
          return { suggestions: [] };
        }
        var word = model.getWordUntilPosition(position);
        var range = {
          startLineNumber: position.lineNumber,
          endLineNumber: position.lineNumber,
          startColumn: word.startColumn,
          endColumn: word.endColumn
        };

        var suggestions = this.phylosModelState.phylosModel.register.map(k => {
          return {
            label: k.name,
            kind: monaco.languages.CompletionItemKind.Constant,
            insertText: k.name,
            documentation: 'Value of Register ' + k.name
          }
        }).concat([

          {
            label: 'addi',
            kind: monaco.languages.CompletionItemKind.Snippet,
            insertText: 'addi ${0:r1} ${1:r2} ${2:1}',
            insertTextRules: monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
            documentation: 'Addi Operator'
          },
          {
            label: 'MULT',
            kind: monaco.languages.CompletionItemKind.Operator,
            insertText: 'MULT'
          },
          {
            label: 'DIV',
            kind: monaco.languages.CompletionItemKind.Operator,
            insertText: 'DIV'
          }] as any);
        return { suggestions: suggestions };
      }
    } as any);

    //hover provider in debug mode
    //to show register contents
    monaco.languages.registerHoverProvider('phylos', {
      provideHover: (model: monaco.editor.ITextModel, position: monaco.Position): monaco.languages.ProviderResult<monaco.languages.Hover> => {
        if (this.phylosModelState.phylosModel.isDebugMode == PhylosDebugState.debugBreak) {
          //shift to find savely match
          (position.column as any) -= 2;
          var matches = model.findNextMatch("r([0-9]+)", position, true, false, "0", true);

          if (matches && matches.range.startLineNumber == position.lineNumber && matches.range.getStartPosition().column - 5 < position.column) {
            var register = matches.matches[1];
            var r = this.phylosModelState.phylosModel.register[parseInt(register)];
            if (r) {
              return {
                range: matches.range,
                contents: [
                  { value: '**R[' + register + ']**: ' + r.value }
                ]
              }
            }
          }
        }
        return null;
      }
    });

    //format document provider
    monaco.languages.registerDocumentFormattingEditProvider('phylos', {
      provideDocumentFormattingEdits: (model: monaco.editor.ITextModel, options, token): monaco.languages.TextEdit[] => {
        //find largest jump mark
        var largespace: number = 0;
        model.getLinesContent().forEach((l, i) => {
          if (/^ *([A-Za-z][A-Za-z0-9]*):/.test(l)) {
            var len = /^ *([A-Za-z][A-Za-z0-9]*):/.exec(l)[1].length;
            if (largespace < len) {
              largespace = len;
            }
          }
        });
        //create padding string
        var paddingString: string = "";
        for (let index = 0; index <= largespace; index++) {
          paddingString += " ";
        }
        var paddingString2: string = "                                                 " + paddingString;
        //create modifications
        var ret: monaco.languages.TextEdit[] = [];
        model.getLinesContent().forEach((l, i) => {
          if (/^\s*#((?!\#).)*/.test(l)) {
            //is comment line
            //remove only start whitespaces
            l = l.replace(/^\s*#/, '#');
          } else {
            if (/^ *([A-Za-z][A-Za-z0-9]*):/.test(l)) {
              //jump mark at beginning
              l = l.replace(/^\s*/, '');
              var res = /[A-Za-z][A-Za-z0-9]*:/.exec(l)[0];
              var split = l.split(/^[A-Za-z][A-Za-z0-9]*:/);
              split[1] = split[1].split(/\s{2,}/).join(' ');
              l = split.join(res + paddingString.substring(0, paddingString.length - res.length));
            } else {
              l = paddingString + l.split(/\s{2,}/).join(' ');
            }
            //end line comments
            if (/((?!\#).)*(#((?!\#).)*)$/.test(l)) {
              var res = /(#((?!#).)*)$/.exec(l)[0];
              var split = l.split(/(#((?!#).)*)$/);
              l = l.replace(/(#((?!#).)*)$/, paddingString2.substring(0, paddingString2.length - split[0].length) + res);
            }
          }



          ret.push({
            range: new monaco.Range(i + 1, 0, i + 1, Number.MAX_SAFE_INTEGER),
            text: l
          })
        })
        return ret;
      }
    })

    //restore breakpoints first
    this.phylosModelState.phylosModel.breakpoints.forEach(b => {
      this.decorations.push({
        range: new monaco.Range(b, 1, b, 1),
        options: {
          isWholeLine: false,
          className: '',
          glyphMarginClassName: 'material-icons break-point',
          glyphMarginHoverMessage: { value: 'toggle breakpoint' }
        }

      });
    })
    this.decorationReference = this.editor.deltaDecorations(this.decorationReference, this.decorations);

    //register events
    //set break points
    this.editor.onMouseDown(e => {
      const data = e.target.detail;
      if ((e.target.type !== monaco.editor.MouseTargetType.GUTTER_GLYPH_MARGIN && e.target.type !== monaco.editor.MouseTargetType.GUTTER_LINE_NUMBERS) || data.isAfterLines) {
        return;
      }

      //set/unset break points
      if (this.phylosModelState.phylosModel.breakpoints.has(e.target.position.lineNumber)) {
        this.phylosModelState.phylosModel.breakpoints.delete(e.target.position.lineNumber);
        //remove decoration
        this.decorations = this.decorations.filter(d => d.range.startLineNumber != e.target.position.lineNumber || d.options.glyphMarginClassName.indexOf('break-point') == -1);
      } else {
        this.phylosModelState.phylosModel.breakpoints.add(e.target.position.lineNumber);
        //add decoration
        this.decorations.push({
          range: new monaco.Range(e.target.position.lineNumber, 1, e.target.position.lineNumber, 1),
          options: {
            isWholeLine: false,
            className: '',
            glyphMarginClassName: 'material-icons break-point',
            glyphMarginHoverMessage: { value: 'toggle breakpoint' }
          }

        });
      }

      //set decorations
      this.decorationReference = this.editor.deltaDecorations(this.decorationReference, this.decorations);
    });

    //set saved erros
    (window as any).monaco.editor.setModelMarkers(this.editor.getModel(), '', this.phylosModelState.phylosModel.errorList.map(e => {
      return {
        startLineNumber: e.lineNumber,
        startColumn: 0,
        endLineNumber: 2,
        endColumn: Number.MAX_SAFE_INTEGER,
        message: e.errorMsg,
        severity: e.errorType == "error" ? monaco.MarkerSeverity.Error : e.errorType == "info" ? monaco.MarkerSeverity.Info : monaco.MarkerSeverity.Warning
      }
    }))
    //on syntax error add editor message
    this.phylosModelState.phylosModel.errorListChange.subscribe(err => {
      (window as any).monaco.editor.setModelMarkers(this.editor.getModel(), '', err.map(e => {
        return {
          startLineNumber: e.lineNumber,
          startColumn: 0,
          endLineNumber: 2,
          endColumn: Number.MAX_SAFE_INTEGER,
          message: e.errorMsg,
          severity: e.errorType == "error" ? monaco.MarkerSeverity.Error : e.errorType == "info" ? monaco.MarkerSeverity.Info : monaco.MarkerSeverity.Warning
        }
      }))
    });

    //resize monaco editor on component resize
    this.container.on('resize', () => {
      this.editor.layout();
    });

    //the initial theme
    this.setEditorTheme(this.phylosModelState.currentTheme);

    //subscribe to theme changes
    this.phylosModelState.currentThemeChange.subscribe(t => {
      this.setEditorTheme(t);
    });

    //add debugging shortcut
    //run
    this.editor.addCommand(monaco.KeyCode.F5, () => {
      //start debugging
      this.stopDebugging();
      this.startDebugging();
    });
    //step over
    this.editor.addCommand(monaco.KeyCode.F10, () => {
      if (this.phylosModelState.phylosModel.isDebugMode == PhylosDebugState.debugBreak) {
        this.nextStep();
      }
    });
    //run to next jump
    this.editor.addCommand(monaco.KeyCode.F11, () => {
      if (this.phylosModelState.phylosModel.isDebugMode == PhylosDebugState.debugBreak) {
        this.nextJumpMark();
      }
    });
    //resume | pause
    this.editor.addCommand(monaco.KeyMod.CtrlCmd | monaco.KeyMod.Shift | monaco.KeyCode.F5, () => {
      if (this.phylosModelState.phylosModel.isDebugMode == PhylosDebugState.debugBreak) {
        this.resumeDebug();
      } else if (this.phylosModelState.phylosModel.isDebugMode == PhylosDebugState.debugRunning) {
        this.pauseDebugging();
      }
    });
    //stop
    this.editor.addCommand(monaco.KeyMod.Shift | monaco.KeyCode.F5, () => {
      if (this.phylosModelState.phylosModel.isDebugMode != PhylosDebugState.noDebug) {
        this.stopDebugging();
      }
    });

    //some context menu entries
    //undo
    this.editor.addAction({
      // An unique identifier of the contributed action.
      id: 'phylos_1',

      // A label of the action that will be presented to the user.
      label: 'Undo',

      // An optional array of keybindings for the action.
      keybindings: [
      ],

      // A precondition for this action.
      precondition: null,

      // A rule to evaluate on top of the precondition in order to dispatch the keybindings.
      keybindingContext: null,

      contextMenuGroupId: 'navigation',

      contextMenuOrder: 1.5,

      // Method that will be executed when the action is triggered.
      // @param editor The editor instance is passed in as a convinience
      run: (ed) => {
        ed.trigger('contextmenu', 'undo', null)
      }
    });
    //redo
    this.editor.addAction({
      // An unique identifier of the contributed action.
      id: 'phylos_2',

      // A label of the action that will be presented to the user.
      label: 'Redo',

      // An optional array of keybindings for the action.
      keybindings: [
      ],

      // A precondition for this action.
      precondition: null,

      // A rule to evaluate on top of the precondition in order to dispatch the keybindings.
      keybindingContext: null,

      contextMenuGroupId: 'navigation',

      contextMenuOrder: 1.6,

      // Method that will be executed when the action is triggered.
      // @param editor The editor instance is passed in as a convinience
      run: (ed) => {
        ed.trigger('contextmenu', 'redo', null)
      }
    });


    //listen for undo redo events from edit menu
    this.phylosModelState.undoRedoRequested.subscribe(ur => {
      this.editor.trigger('controller', ur ? "redo" : "undo", null);
    })

  }

  /**
   * Method for starting the debug mode
   */
  startDebugging(): void {
    if (this.phylosModelState.phylosModel.errorList.length > 0) {
      alert('Please resolve all errors first.');
      return;
    }

    //disable editing
    this.editorOptions.readOnly = true;
    this.editor.updateOptions(this.editorOptions);

    //set debug active
    this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugRunning;
    //change line number to 0
    this.phylosModelState.phylosModel.currentDebugLine = 0;
    this.codeLines = this.phylosModelState.phylosModel.code.split(/\r?\n/);

    //get jumpMarks
    this.jumpMarks.clear();
    for (var i = 0; i < this.codeLines.length; i++) {
      if (/^ *([A-Za-z][A-Za-z0-9]*):/.test(this.codeLines[i])) {
        this.jumpMarks.set(this.codeLines[i].match(/^ *([A-Za-z][A-Za-z0-9]*):/)[1], i);
      }
    }

    //iterate over
    //use recursive iteration with sleeps because js is single threaded and the gui have to work too
    var iterator = () => setTimeout(() => {
      this.makeOneDebugStep();
      if ((this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.debugRunning) {
        iterator();
      }
    }, 10);

    if ((this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.debugRunning) {
      iterator();
    }
  }

  /**
   * Resumes debugging
   */
  resumeDebug(): void {
    //remove current line decoration
    this.generateEditorDecorationForCurrentLine(true);

    this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugRunning;

    this.makeOneDebugStep(false, true);

    //iterate over
    //use recursive iteration with sleeps because js is single threaded and the gui have to work too
    var iterator = () => setTimeout(() => {
      this.makeOneDebugStep();
      if ((this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.debugRunning) {
        iterator();
      }
    }, 10);

    if ((this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.debugRunning) {
      iterator();
    }
  }

  /**
   * Runs until next break point or jump.
   */
  nextJumpMark(): void {
    //remove current line decoration
    this.generateEditorDecorationForCurrentLine(true);

    this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugRunning;

    this.makeOneDebugStep(true, true);

    //iterate over
    //use recursive iteration with sleeps because js is single threaded and the gui have to work too
    var iterator = () => setTimeout(() => {
      this.makeOneDebugStep(true);
      if ((this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.debugRunning) {
        iterator();
      }
    }, 10);

    if ((this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.debugRunning) {
      iterator();
    }
  }

  /**
   * Does next step
   */
  nextStep(): void {
    //remove current line decoration
    this.generateEditorDecorationForCurrentLine(true);

    this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugRunning;

    //make step
    this.makeOneDebugStep(false, true);
    //if current line is comment line
    this.makeOneDebugStep(false, true, true);

    //check wheter its already breaked
    if ((this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.debugRunning) {
      this.generateEditorDecorationForCurrentLine();
      this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugBreak;
    }
  }

  /**
   * Stops/ interupts Debugging
   */
  stopDebugging(): void {
    this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.noDebug;

    //remove line decorations
    this.decorations = this.decorations.filter(d => d.options.className != 'debug-line');

    this.decorationReference = this.editor.deltaDecorations(this.decorationReference, this.decorations);

    //enable editing
    this.editorOptions.readOnly = false;
    this.editor.updateOptions(this.editorOptions);
  }

  /**
   * Pauses debugging
   */
  pauseDebugging(): void {
    this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugBreak;

    setTimeout(() => {
      this.generateEditorDecorationForCurrentLine();
    }, 300);
  }

  /**
   * Genrate currentLineDecoration
   * @param remove true for removing the line decoration, otherwise add
   */
  private generateEditorDecorationForCurrentLine(remove: boolean = false): void {
    //current line decoration
    if (remove) {
      //remove decoration
      this.decorations = this.decorations.filter(d => d.range.startLineNumber != this.phylosModelState.phylosModel.currentDebugLine + 1 || d.options.className != 'debug-line');
    } else {
      //add decoration
      this.decorations.push({
        range: new monaco.Range(this.phylosModelState.phylosModel.currentDebugLine + 1, 1, this.phylosModelState.phylosModel.currentDebugLine + 1, 1),
        options: {
          isWholeLine: true,
          className: 'debug-line',
          glyphMarginClassName: 'material-icons debug-sign'
        }
      });
    }
    this.decorationReference = this.editor.deltaDecorations(this.decorationReference, this.decorations);
  }

  /**
   * Internal function for running the current line
   * breaks if last line reached
   * @param breakOnJump true, for break on jump
   * @param ignoreBreakpoint true, for ignoring breakpoints in this step
   * @param tryRun runs until a valid line was found
   * @returns true - if no break point or error reached, false otherwise
   */
  private makeOneDebugStep(breakOnJump: boolean = false, ignoreBreakpoint: boolean = false, tryRun: boolean = false): boolean {
    //check whether we can do this step
    if (this.phylosModelState.phylosModel.currentDebugLine >= this.codeLines.length || (this.phylosModelState.phylosModel.isDebugMode as PhylosDebugState) == PhylosDebugState.noDebug) {
      this.stopDebugging();
      return true;
    }


    //get line and remove comments and jump marks
    var line = this.codeLines[this.phylosModelState.phylosModel.currentDebugLine].split(/\#.*?\#/).join('').replace(/\#.*/, '').replace(/^ *([A-Za-z][A-Za-z0-9]*):/, '');
    if (!(/^( |\t)*$/.test(line))) {
      //contains code
      if (tryRun) {
        return true;
      }

      //check for break point
      if (!ignoreBreakpoint && this.phylosModelState.phylosModel.breakpoints.has(this.phylosModelState.phylosModel.currentDebugLine + 1)) {
        this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugBreak;
        //generate decorations
        this.generateEditorDecorationForCurrentLine();
        return false;
      }

      try {
        //run trunsition function
        var res = PhylosModel.modelTransitions.get(line.split(/\t/).join(' ').match(/^ *([A-Za-z]+)/)[1])({
          line: line,
          ram: this.phylosModelState.phylosModel.RAM,
          register: this.phylosModelState.phylosModel.register,
          ioregister: this.phylosModelState.phylosModel.IORegister
        });
        if (res.abort) {
          this.stopDebugging();
          return false;
        }

        if (res.jump) {
          if (this.jumpMarks.has(res.jumpMark)) {
            this.phylosModelState.phylosModel.currentDebugLine = this.jumpMarks.get(res.jumpMark);
            //check whether we should until next jump
            if (breakOnJump) {
              this.phylosModelState.phylosModel.isDebugMode = PhylosDebugState.debugBreak;
              //generate decorations
              this.generateEditorDecorationForCurrentLine();
              return false;
            }
            return true;
          } else {
            throw "Jump mark " + res.jumpMark + " not found";
          }
        }
      } catch (error) {
        //abort debugging because of runtime error
        alert('Runtime-Error: ' + error);
        console.error(error);
        this.phylosModelState.phylosModel.errorList.push({
          errorMsg: error,
          errorType: "error",
          lineNumber: this.phylosModelState.phylosModel.currentDebugLine + 1
        });
        this.phylosModelState.phylosModel.errorListChange.next(this.phylosModelState.phylosModel.errorList);
        this.stopDebugging();
        return false;
      }
      this.phylosModelState.phylosModel.currentDebugLine++;
    } else {
      //was empty this line so run again
      this.phylosModelState.phylosModel.currentDebugLine++;
      return this.makeOneDebugStep(breakOnJump, ignoreBreakpoint, tryRun);
    }

    return true;
  }

}
