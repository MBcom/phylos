import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PhylosRegister } from 'src/app/models/phylos-register';
import { MatSort } from '@angular/material/sort';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';
import { PhylosDebugState } from 'src/app/enums/phylos-debug-state.enum';
import { PhylosInstruction } from 'src/app/models/phylos-instruction';

@Component({
  selector: 'app-rom',
  templateUrl: './rom.component.html',
  styleUrls: ['./rom.component.scss']
})
export class RomComponent implements OnInit {
  

  //displayed columns in table
  displayedColumns: string[] = ['address','opcode','command'];

  //displayed data
  dataSource = new MatTableDataSource<PhylosInstruction>();

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  //last debug state
  lastDebugState: PhylosDebugState;

  constructor(
    public phylosModelState: PhylosModelStateService
  ) { 
    this.lastDebugState = phylosModelState.phylosModel.isDebugMode;
    if (this.lastDebugState == PhylosDebugState.debugRunning){
      this.generateInstructionSet();
    }
  }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  /**
   * Generate instruction set when debugging starts
   */
  ngAfterViewChecked(): void {
    if (this.phylosModelState.phylosModel.isDebugMode != PhylosDebugState.noDebug && this.lastDebugState == PhylosDebugState.noDebug){
      this.generateInstructionSet();
    }
    this.lastDebugState = this.phylosModelState.phylosModel.isDebugMode;
  }

  /**
   * Method for generating the instructionset
   */
  generateInstructionSet(): void{
    var ret: PhylosInstruction[] = [];
    this.phylosModelState.phylosModel.code.split(/\r?\n/).forEach((l,i) => {
      if (/^\s*#((?!\#).)*/.test(l)){
        //for whole line comments add nop s
        ret.push({
          address: i,
          command: "NOP",
          opcode: 0
        })
      }else{
        l = l.replace(/^ *([A-Za-z][A-Za-z0-9]*):/,'').split(/\t/).join(' ').split(/#.*#/).join('').replace(/#.*/,'').split(/\s{2,}/).join(' ').replace(/^\s*/,'').replace(/ *$/,'');
        if (l.length == 0){
          ret.push({
            address: i,
            command: "NOP",
            opcode: 0
          })
        }else{
          ret.push({
            address: i,
            command: l,
            opcode: /*use hash as opcode*/ l.split(' ')[0].split('').reduce((a,b)=>{a=((a<<5)-a)+b.charCodeAt(0);return a&a},0)
          })
        }
      }
    })
    this.dataSource.data = ret;
  }

}
