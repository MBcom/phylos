import { Component, OnInit } from '@angular/core';
import { PhylosModelStateService } from 'src/app/services/phylos-model-state.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string = '';
  password: string = '';
  errorMsg: string = '';

  constructor(
    private phylosState: PhylosModelStateService
  ) { }

  ngOnInit() {
  }

  /**
   * Trys to log user in
   */
  Login(): void {
    if (!this.phylosState.Login(this.username, this.password)) {
      this.errorMsg = 'Username and/ or password is wrong';
    }
  }
}
