import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { imports, declarations } from './app.module';
import { PhylosModelStateService } from './services/phylos-model-state.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: declarations,
      imports: imports,
      providers: [
        PhylosModelStateService
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

});
