import { PhylosModel } from './phylos-model';
import { PhylosDebugState } from '../enums/phylos-debug-state.enum';

describe('PhylosModel', () => {
  it('should create an instance', () => {
    expect(new PhylosModel()).toBeTruthy();
  });

  it('should initialize correctly', () => {
    var m = new PhylosModel();
    expect(m.IORegister).toBeDefined();
    expect(m.IORegister.length).toEqual(11);
    expect(m.currentDebugLine).toEqual(0);
    expect(m.isDebugMode).toEqual(PhylosDebugState.noDebug);
    expect(m.register).toBeDefined();
    expect(m.register.length).toEqual(32);
  });

  it('should serialize correctly', () => {
    var m = new PhylosModel();
    m.RAM.set(1,2);
    m.code = '#';
    expect(m.toJson()).toEqual(JSON.stringify({
      code: m.code,
      RAM: [[1,2]],
      register: m.register,
      breakpoints: [],
      IORegister: m.IORegister
    }));
    m.breakpoints.add(5);
    expect(m.toJson()).toEqual(JSON.stringify({
      code: m.code,
      RAM: [[1,2]],     
      register: m.register,
      breakpoints: [5],
      IORegister: m.IORegister
    }));
  })
});
