import { PhylosRamMap } from './phylos-ram-map';

describe('PhylosRamMap', () => {
  it('should create an instance', () => {
    expect(new PhylosRamMap()).toBeTruthy();
  });

  it('should return a valid ram map',() => {
    var m = new PhylosRamMap();
    m.set(1,2);
    m.set(50,4);
    m.set(52,1);
    expect(m.getRamView()).toEqual([
      {
        isSpace: false,
        name: 0,
        value: 0
      },
      {
        isSpace: false,
        name: 1,
        value: 2
      },
      {
        isSpace: false,
        name: 2,
        value: 0
      },
      {
        isSpace: false,
        name: 3,
        value: 0
      },
      {
        isSpace: false,
        name: 4,
        value: 0
      },
      {
        isSpace: false,
        name: 5,
        value: 0
      },
      {
        isSpace: false,
        name: 6,
        value: 0
      },
      {
        isSpace: false,
        name: 7,
        value: 0
      },
      {
        isSpace: false,
        name: 8,
        value: 0
      },
      {
        isSpace: false,
        name: 9,
        value: 0
      },
      {
        isSpace: true,
        value: 40
      },
      {
        isSpace: false,
        value: 4,
        name: 50
      },
      {
        isSpace: true,
        value: 1
      },
      {
        isSpace: false,
        value: 1,
        name: 52
      },
      {
        isSpace: true,
        value: 1.7976931348623157e+308
      }
    ])
  })
});
